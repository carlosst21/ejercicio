import java.util.ArrayList;


public class Empresa {
	
	private String RazonSocial;
	private String direccion;
	private ArrayList<Contacto> contactos = new ArrayList<Contacto>();
	
	public Empresa() {
		super();
	}
		
	public Empresa(String razonSocial, String direccion) {
		super();
		RazonSocial = razonSocial;
		this.direccion = direccion;
	}

	public void addContacto(String nombre, String telefono, String direccion, Integer ciudad, String mail) {
		Contacto contacto = new Contacto( nombre, telefono, direccion, ciudad, mail);
		this.contactos.add(contacto);
		return;
	}

	public String getRazonSocial() {
		return RazonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		RazonSocial = razonSocial;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public ArrayList<Contacto> getContactos() {
		return contactos;
	}

	public Integer countContactos() {
		return this.getContactos().size();
	}
	
	
	
	
}
