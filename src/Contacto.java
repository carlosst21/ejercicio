
public class Contacto {

	private String nombre;
	private String telefono;
	private String direccion;
	private Integer ciudad;
	private String mail;
	private String ciudades[] = new String[] {"Buenos Aires", "Cordoba", "Santa Fe", "Mendoza", "Parana", "Otra"};
	
	public Contacto() {
		super();
	}

	public Contacto(String nombre, String telefono, String direccion, Integer ciudad, String mail) {
		super();
		this.nombre = nombre;
		this.telefono = telefono;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.mail = mail;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Integer getCiudad() {
		return ciudad;
	}
	
	public String getCiudadNombre() {
		String nombreCiudad="";
		int ciclo=0;
		for(ciclo=0;ciclo<ciudades.length;ciclo++) {
			if(ciclo+1==this.getCiudad()) {
				nombreCiudad = ciudades[ciclo].toString();
				break;
			}
		}
		return nombreCiudad;
	}

	public void setCiudad(Integer ciudad) {
		this.ciudad = ciudad-1;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
	
}
