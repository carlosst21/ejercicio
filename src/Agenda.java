
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Agenda {


	public static void main(String[] args) {
		ArrayList<Empresa> agenda = new ArrayList<Empresa>();
		Integer intEmpresa =0;
		int ciclo = 0;
		
		try {
			BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
			String texto = "";
			char opcion = '1';
			while ((opcion == '1') || (opcion == '2') || (opcion == '3') || (opcion == '4') || (opcion == '5')
					|| (opcion == '6')) {
				System.out.println(" ");
				System.out.println("AGENDA");
				System.out.println("======");
				System.out.println("1-Nueva Empresa");
				System.out.println("2-Nuevo Contacto");
				System.out.println("3-Buscar Contacto por Nombre");
				System.out.println("4-Buscar Contacto por Ciudad");
				System.out.println("5-Buscador Complejo");
				System.out.println("6-Listado de Empresas");
				System.out.println("0-Salir");
				System.out.println(" ");
				System.out.print("Opci�n: ");
				texto = teclado.readLine();
				opcion = texto.charAt(0);

				switch (opcion) {
				case '1':
					System.out.println("Nueva Empresa:");
					String razonSocial = "";
					String direccionEmpresa = "";
					System.out.println("Introduzca la Razon Social:");
					razonSocial = teclado.readLine();
					System.out.println("Introduzca la Direccion:");
					direccionEmpresa = teclado.readLine();
					if (!razonSocial.trim().isEmpty()) {
						Empresa empresa = new Empresa(razonSocial, direccionEmpresa);
						agenda.add(empresa);
					} else {
						System.out.println("ERROR -- No ingreso Razon Social");
					}
					break;
				case '2':
					System.out.println("Nuevo Contacto:");
					if (agenda.size() > 0) {
						System.out.println("Nuevo Contacto:");
						System.out.println("Ingrese a que Empresa Agrega ese nuevo Contacto: ");
						for (ciclo=0; ciclo<agenda.size(); ciclo++) {
							System.out.println(ciclo+1 + " - " + agenda.get(ciclo).getRazonSocial());
						}
						System.out.println("Ingrese Opcion:");
						String nroEmpresa = teclado.readLine();
						String nombre= "";
						String telefono= "";
						String direccionContacto= "";
						Integer ciudad=0;
						String srtCiudad="";
						String mail= "";
						
						System.out.println("Introduzca Nombre:");
						nombre = teclado.readLine();
						System.out.println("Introduzca la Direccion:");
						direccionContacto = teclado.readLine();
						System.out.println("Introduzca Telefono:");
						telefono = teclado.readLine();
						System.out.println("Introduzca Ciudad:");
						System.out.println("1 - Buenos Aires / 2 - Cordoba / 3 - Santa Fe / 4 - Mendoza / 5 - Parana / 6 - Otra");
						System.out.println("Ingrese Opcion:");
						srtCiudad = teclado.readLine();
						System.out.println("Introduzca Mail:");
						mail = teclado.readLine();
						if (esNumero(nroEmpresa)) {
							intEmpresa = Integer.parseInt(nroEmpresa);
							if (intEmpresa>0 && intEmpresa<agenda.size()+1) {
								if(!nombre.trim().isEmpty()){
									if(esNumero(srtCiudad)) {
										ciudad = Integer.parseInt(srtCiudad);
										if (ciudad>0 && ciudad<7) {
											agenda.get(intEmpresa-1).addContacto(nombre, telefono, direccionContacto, ciudad, mail);
										} else {
											System.out.println("ERROR - El Nro ingresado no corresponde a una ciudad");
										}
									} else {
										System.out.println("ERROR - Debe ingresar un nro de ciudad");
									}
								} else {
									System.out.println("ERROR - Debe ingresar un Nombre");
								}
							} else {
								System.out.println("ERROR - El Nro ingresado no corresponde a una empresa");
							}
						} else {
							System.out.println("ERROR - Debe ingresar un nro de empresa");
						}
					} else {
						System.out.println("Debe ingresar una Empresa primero");
					}
					break;
				case '3':
					System.out.println("Buscar Contacto por Nombre:");
					break;
				case '4':
					System.out.println("Buscar Contacto por Ciudad:");
					break;
				case '5':
					System.out.println("Buscador Complejo:");
					break;
				case '6':
					System.out.println("Listado de Empresas:");
					ciclo = 0;
					for (ciclo = 0; ciclo < agenda.size(); ciclo++) {
						System.out.println("Empresa: " + agenda.get(ciclo).getRazonSocial() + "  - Direccion: "
								+ agenda.get(ciclo).getDireccion() + " (" + agenda.get(ciclo).countContactos()
								+ " contactos)");
					}
					break;
				case '0':
					System.out.println("Ha salido del programa");
					break;
				default:
					System.out.println("Opci�n incorrecta ...");
					opcion = '1';
				}
			}
		} catch (IOException ex) {
			System.out.println("Se produjo un error: " + ex.getMessage());
		}
	}

	public static boolean esNumero(String str) {
		boolean result =  str.matches("[+-]?\\d*(\\.\\d+)?");
		return result;
	}

}
